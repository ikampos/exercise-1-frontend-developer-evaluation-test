angular.module('driversApp', [])
  .controller('driversPage', function ($scope, $http) {
    'use strict';

    $scope.orderConcep = "ranking";

    $http.get('data.json').success(function (data) {
      $scope.drivers = data.drivers;
    });

  });
